//
//  ApplicationAssembly.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation
import Swinject

final class ApplicationAssembly: Assembly {

  static let assembler: Assembler = {
    return Assembler(
      [
        CoreLayerAssembly(),
        BusinessLogicLayerAssembly(),
        ApplicationAssembly()
      ]
    )
  }()

  private init() {}

  func assemble(container: Container) {

  }
}

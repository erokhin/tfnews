//
//  AppDelegate.swift
//  TFNews
//
//  Created by erokhin on 11/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions
    launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    configureCoreDataStack()
    return true
  }

  private func configureCoreDataStack() {
    let resolver = ApplicationAssembly.assembler.resolver
    let coreDataStack = resolver.resolve(CoreDataStack.self)!
    _ = coreDataStack.load()
  }
}

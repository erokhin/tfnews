//
//  ArticlesCacheServiceProvider.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation
import CoreData

final class ArticlesCacheServiceProvider: ArticlesCacheService {

  let stack: CoreDataStack

  init(stack: CoreDataStack) {
    self.stack = stack
  }

  func addPreviews(_ articles: [Article], reset: Bool, completion: @escaping (Result<Void>) -> Void) {
    stack.perform(on: .background) { context in
      do {
        let req: NSFetchRequest<ArticleMO> = ArticleMO.fetchRequest()
        req.fetchBatchSize = 20
        req.includesPropertyValues = false
        if reset {
          req.predicate = NSPredicate(format: "isHidden = false")
          try context.fetch(req).forEach {
            $0.isHidden = true
          }
        }
        try articles.forEach {
          req.predicate = NSPredicate(format: "id = %@", $0.id)
          if let obj = try context.fetch(req).first {
            obj.update($0)
          } else {
            ArticleMO(entity: ArticleMO.entity(), insertInto: context).update($0)
          }
        }

        if context.hasChanges {
          try context.save()
        }

        DispatchQueue.main.async {
          completion(.success(()))
        }
      } catch {
        DispatchQueue.main.async {
          completion(.failure(error))
        }
      }
    }
  }

  func getArticle(slug: String) -> ArticleMO? {
    let request: NSFetchRequest<ArticleMO> = ArticleMO.fetchRequest()
    request.predicate = NSPredicate(format: "slug = %@", slug)
    request.fetchLimit = 1
    do {
      return try stack.mainContext.fetch(request).first
    } catch {
      print(error.localizedDescription)
      return nil
    }
  }

  func createOrUpdate(id: String, block: @escaping (ArticleMO) -> Void,
                      completion: ((Result<ArticleMO>) -> Void)?) {
    stack.perform(on: .background) { [weak stack] context in
      do {
        let request: NSFetchRequest<ArticleMO> = ArticleMO.fetchRequest()
        request.predicate = NSPredicate(format: "id = %@", id)
        let object = try context.fetch(request).first ?? ArticleMO(entity: ArticleMO.entity(), insertInto: context)
        block(object)
        if context.hasChanges {
          try context.save()
        }
        let objectID = object.objectID
        guard completion != nil else { return }
        DispatchQueue.main.async {
          (stack?[.main].object(with: objectID) as? ArticleMO).map {
            completion!(.success($0))
          }
        }
      } catch {
        guard completion != nil else { return }
        DispatchQueue.main.async {
          completion!(.failure(error))
        }
      }
    }
  }

  func update(_ article: ArticleMO, block: @escaping (ArticleMO) -> Void,
              completion: ((Result<ArticleMO>) -> Void)?) {
    let objectID = article.objectID
    stack.perform(on: .background) { [weak stack] context in
      (context.object(with: objectID) as? ArticleMO).map(block)
      do {
        if context.hasChanges {
          try context.save()
        }
        guard completion != nil else { return }
        DispatchQueue.main.async {
          (stack?[.main].object(with: objectID) as? ArticleMO).map {
            completion!(.success($0))
          }
        }
      } catch {
        guard completion != nil else { return }
        DispatchQueue.main.async {
          completion!(.failure(error))
        }
      }
    }
  }
}

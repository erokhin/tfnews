//
//  ArticlesCacheService.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

protocol ArticlesCacheService: class {
  func addPreviews(_ articles: [Article], reset: Bool, completion: @escaping (Result<Void>) -> Void)

  func getArticle(slug: String) -> ArticleMO?

  func createOrUpdate(id: String, block: @escaping (ArticleMO) -> Void,
                      completion: ((Result<ArticleMO>) -> Void)?)

  func update(_ article: ArticleMO, block: @escaping (ArticleMO) -> Void,
              completion: ((Result<ArticleMO>) -> Void)?)
}

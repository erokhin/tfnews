//
//  ArticlesServiceProvider.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class ArticlesServiceProvider: ArticlesService {

  let api: APIClient
  let queue: DispatchQueue = DispatchQueue.main

  init(api: APIClient) {
    self.api = api
  }

  func loadArticles(offset: Int, count: Int, completion: @escaping (Result<ArticlesPage>) -> Void) -> Cancelable {
    return api.perform(ArticlesRequest(offset: offset, count: count), on: queue, completion: completion)
  }

  func loadArticle(slug: String, completion: @escaping (Result<Article>) -> Void) -> Cancelable {
    return api.perform(ArticleRequest(slug: slug), on: queue, completion: completion)
  }
}

//
//  ArticlesService.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

protocol ArticlesService: class {
  func loadArticles(offset: Int, count: Int, completion: @escaping (Result<ArticlesPage>) -> Void) -> Cancelable
  func loadArticle(slug: String, completion: @escaping (Result<Article>) -> Void) -> Cancelable
}

//
//  BusinessLogicLayerAssembly.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation
import Swinject

final class BusinessLogicLayerAssembly: Assembly {
  func assemble(container: Container) {
    container.register(ArticlesService.self) { resolver in
      return ArticlesServiceProvider(api: resolver.resolve(APIClient.self)!)
    }
    container.register(ArticlesCacheService.self) { resolver in
      return ArticlesCacheServiceProvider(stack: resolver.resolve(CoreDataStack.self)!)
    }
  }
}

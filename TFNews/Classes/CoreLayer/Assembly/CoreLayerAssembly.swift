//
//  CoreLayerAssembly.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation
import Swinject
import CoreData

final class CoreLayerAssembly: Assembly {
  func assemble(container: Container) {
    container.register(APIClient.self) { _ in
      return APIClientImpl()
    }.inObjectScope(.container)

    container.register(CoreDataStack.self) { _ in
      return CoreDataStack()
    }.inObjectScope(.container)
  }
}

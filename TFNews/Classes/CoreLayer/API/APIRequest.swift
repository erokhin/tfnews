//
//  APIRequest.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

extension DateFormatter {
  static let articles: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    return formatter
  }()
}

protocol APIRequest {
  associatedtype ReturnType
  var method: HTTPMethod { get }
  var path: String { get }
  var params: Parameters? { get }

  func handle(data: Data, response: HTTPURLResponse) throws -> ReturnType
}

extension APIRequest {
  var method: HTTPMethod { return .get }
}

extension APIRequest where ReturnType: Decodable {
  func handle(data: Data, response: HTTPURLResponse) throws -> ReturnType {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .formatted(DateFormatter.articles)
    let response = try decoder.decode(Response<ReturnType>.self, from: data)
    return response.result
  }
}

//
//  APIResponse.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

struct Response<T: Decodable>: Decodable {
  let result: T

  enum CodingKeys: String, CodingKey {
    case error
    case response
  }

  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    do {
      result = try values.decode(T.self, forKey: .response)
    } catch {
      guard let errorInfo = try? values.decode(ErrorInfo.self, forKey: .error) else {
        throw error
      }
      throw APIError(info: errorInfo)
    }
  }
}

//
//  ArticlesRequest.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

struct ArticlesRequest: APIRequest {

  typealias ReturnType = ArticlesPage

  let path: String = "getArticles"
  let params: Parameters?

  init(offset: Int, count: Int = 20) {
    params = [
      "pageOffset": offset,
      "pageSize": count
    ]
  }
}

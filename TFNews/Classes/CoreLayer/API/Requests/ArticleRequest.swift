//
//  ArticleRequest.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

struct ArticleRequest: APIRequest {

  typealias ReturnType = Article

  let path: String = "getArticle"
  let params: Parameters?

  init(slug: String) {
    params = [
      "urlSlug": slug
    ]
  }
}

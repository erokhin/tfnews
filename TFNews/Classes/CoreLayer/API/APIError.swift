//
//  APIError.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

enum APIError: Error {
  case invalidResponse
  case invalidData
  case invalidRequest
  case notFound
  case unknown(String)
}

struct ErrorInfo: Codable {
  let message: String
  let errcode: String
}

extension APIError {
  init(info: ErrorInfo) {
    switch info.errcode {
    case "EINVALID":
      self = .invalidRequest
    case "ENOTFOUND":
      self = .notFound
    default:
      self = .unknown(info.errcode)
    }
  }
}

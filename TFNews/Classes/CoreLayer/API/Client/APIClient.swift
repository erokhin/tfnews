//
//  APIClient.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

enum Result<T> {
  case success(T)
  case failure(Error)
}

protocol Cancelable: class {
  func cancel()
}

protocol APIClient: class {
  func perform<R: APIRequest>(_ request: R, on queue: DispatchQueue,
                              completion: @escaping (Result<R.ReturnType>) -> Void) -> Cancelable
}

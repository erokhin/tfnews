//
//  APIClientImpl.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

extension URLSessionTask: Cancelable {}

final class APIClientImpl: APIClient {

  let baseUrl = URL(string: "https://cfg.tinkoff.ru/news/public/api/platform/v1/")!
  let session: URLSession

  init() {
    session = URLSession(configuration: .default, delegate: nil, delegateQueue: nil)
  }

  func perform<R: APIRequest>(_ request: R, on queue: DispatchQueue,
                              completion: @escaping (Result<R.ReturnType>) -> Void) -> Cancelable {
    let url = URL(string: request.path, relativeTo: baseUrl)!
    let urlRequest = URLRequestBuilder().request(url: url, method: request.method, params: request.params)
    let task = session.dataTask(with: urlRequest) { data, response, error in
      if let error = error {
        return queue.async {
          completion(.failure(error))
        }
      }
      guard let response = response as? HTTPURLResponse else {
        return queue.async {
          completion(.failure(APIError.invalidResponse))
        }
      }
      guard let data = data else {
        return queue.async {
          completion(.failure(APIError.invalidData))
        }
      }
      do {
        let res = try request.handle(data: data, response: response)
        queue.async {
          completion(.success(res))
        }
      } catch {
        queue.async {
          completion(.failure(error))
        }
      }
    }
    task.resume()
    return task
  }
}

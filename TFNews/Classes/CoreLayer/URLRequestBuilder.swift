//
//  URLRequestBuilder.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
  case get = "GET"
  case post = "POST"
}

typealias Parameters = [String: Any]

private extension CharacterSet {
  static let urlQueryComponentAllowed: CharacterSet = {
    var charSet = CharacterSet.urlQueryAllowed
    charSet.remove(charactersIn: ":#[]@!$&'()*+,;=")
    return charSet
  }()
}

struct URLRequestBuilder {
  func request(url: URL, method: HTTPMethod = .get, params: Parameters? = nil,
               headers: [String: String]? = nil) -> URLRequest {
    let finalUrl: URL
    var headers = headers ?? [:]
    let body: Data?
    switch method {
    case .get:
      var components = URLComponents(url: url, resolvingAgainstBaseURL: true)!
      let percentEncodedQuery = (components.percentEncodedQuery.map({ $0 + "&" }) ?? "") + query(params)
      components.percentEncodedQuery = percentEncodedQuery
      finalUrl = components.url!
      body = nil
    case .post:
      finalUrl = url
      headers["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8"
      body = query(params).data(using: .utf8, allowLossyConversion: false)
    }
    var req = URLRequest(url: finalUrl)
    req.httpMethod = method.rawValue
    req.httpBody = body
    req.allHTTPHeaderFields = headers
    return req
  }

  private func queryComponents(key: String, value: Any) -> [(String, String)] {
    var components: [(String, String)] = []
    if let string = value as? String {
      components.append((escape(key), escape(string)))
    } else if let number = value as? NSNumber {
      components.append((escape(key), escape("\(number)")))
    } else {
      components.append((escape(key), escape("\(value)")))
    }
    return components
  }

  private func query(_ params: Parameters?) -> String {
    guard let params = params, !params.isEmpty else { return "" }
    var components: [(String, String)] = []
    params.keys.sorted(by: <).forEach {
      components += queryComponents(key: $0, value: params[$0]!)
    }
    return components.map({ "\($0)=\($1)" }).joined(separator: "&")
  }

  private func escape(_ string: String) -> String {
    return string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryComponentAllowed) ?? string
  }
}

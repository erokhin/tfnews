//
//  HTMLMarkupFilter.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

struct HTMLMarkupFilter {
  func filter(_ text: String) -> String {
    guard let data = text.data(using: .utf8) else { return text }
    do {
      let text = try NSAttributedString(
        data: data,
        options: [
          .documentType: NSAttributedString.DocumentType.html,
          .characterEncoding: String.Encoding.utf8.rawValue
        ],
        documentAttributes: nil
      )
      return text.string
    } catch {
      return text
    }
  }
}

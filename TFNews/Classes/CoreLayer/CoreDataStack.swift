//
//  CoreDataStack.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation
import CoreData

extension Notification.Name {
  static let coreDataStackReady = Notification.Name(rawValue: "coreDataStackReady")
}

final class CoreDataStack {

  enum LoadingStatus {
    case initial
    case loading
    case loaded
  }

  enum ContextID {
    case main
    case background
  }

  subscript(contextId: ContextID) -> NSManagedObjectContext {
    switch contextId {
    case .main:
      return mainContext!
    case .background:
      return backgroundContext!
    }
  }

  private(set) var loadingStatus: LoadingStatus = .initial

  private(set) var mainContext: NSManagedObjectContext!
  private(set) var backgroundContext: NSManagedObjectContext!

  var isLoaded: Bool {
    return loadingStatus == .loaded
  }

  func perform(on contextId: ContextID, block: @escaping (NSManagedObjectContext) -> Void) {
    let context = self[contextId]
    context.perform {
      block(context)
    }
  }

  func load() -> Bool {
    switch loadingStatus {
    case .initial:
      loadingStatus = .loading
    case .loading:
      return false
    case .loaded:
      return true
    }

    guard let modelUrl = Bundle.main.url(forResource: "TFNews", withExtension: "momd") else {
      fatalError()
    }
    guard let model = NSManagedObjectModel(contentsOf: modelUrl) else {
      fatalError()
    }
    let coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)

    backgroundContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    backgroundContext.persistentStoreCoordinator = coordinator

    mainContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    mainContext.automaticallyMergesChangesFromParent = true
    mainContext.parent = backgroundContext

    let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    guard let docsUrl = urls.last else { fatalError() }
    let storeUrl = URL(fileURLWithPath: "TFNews.sqlite", relativeTo: docsUrl)

    DispatchQueue.global(qos: .utility).async { [weak self] in
      do {
        try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil,
                                           at: storeUrl, options: nil)
      } catch {
        fatalError(error.localizedDescription)
      }
      DispatchQueue.main.async {
        self?.loadingCompleted()
      }
    }
    return false
  }

  private func loadingCompleted() {
    assert(loadingStatus == .loading)
    loadingStatus = .loaded
    NotificationCenter.default.post(name: .coreDataStackReady, object: self)
  }
}

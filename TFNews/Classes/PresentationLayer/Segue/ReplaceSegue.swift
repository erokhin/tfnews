//
//  ReplaceSegue.swift
//  TFNews
//
//  Created by erokhin on 16/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import UIKit

class ReplaceSegue: UIStoryboardSegue {
  override func perform() {
    guard let parent = source.parent else { return }
    guard let containerView = source.view.superview else { return }
    destination.view.frame = containerView.bounds
    destination.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    containerView.addSubview(destination.view)
    parent.addChildViewController(destination)
    destination.view.alpha = 0

    UIView.animate(withDuration: 0.3, animations: {
      self.source.view.alpha = 0
      self.destination.view.alpha = 1
    }, completion: { _ in
      self.source.view.removeFromSuperview()
      self.source.removeFromParentViewController()
    })
  }
}

//
//  ArticleDetailPresenter.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class ArticleDetailPresenter {
  weak var view: ArticleDetailViewInput!
  var router: ArticleDetailRouterInput!
  var interactor: ArticleDetailInteractorInput!

  private let titleDateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.timeStyle = .none
    formatter.dateStyle = .medium
    return formatter
  }()
}

// MARK: - ArticleDetailModuleInput

extension ArticleDetailPresenter: ArticleDetailModuleInput {
  func configure(slug: String) {
    interactor.configure(slug: slug)
  }
}

// MARK: - ArticleDetailViewOutput

extension ArticleDetailPresenter: ArticleDetailViewOutput {
  func didTapRetry() {
    interactor.retryLoading()
  }

  func didTap(url: URL) {
    router.open(url: url)
  }

  func viewDidLoad() {
    interactor.load()
  }
}

// MARK: - ArticleDetailInteractorOutput

extension ArticleDetailPresenter: ArticleDetailInteractorOutput {
  func updateTitle(_ date: Date?) {
    view.updateTitle(date.map { titleDateFormatter.string(from: $0) } ?? "")
  }

  func updateState(_ state: ArticleDetailViewState) {
    view.updateState(state)
  }
}

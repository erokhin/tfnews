//
//  ArticleDetailModuleConfigurator.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

struct ArticleDetailModuleConfigurator {
  func configure(_ viewController: ArticleDetailViewController) {
    let resolver = ApplicationAssembly.assembler.resolver
    let router = ArticleDetailRouter()
    let presenter = ArticleDetailPresenter()
    let interactor = ArticleDetailInteractor()

    presenter.router = router
    presenter.view = viewController
    presenter.interactor = interactor

    interactor.output = presenter
    interactor.articlesService = resolver.resolve(ArticlesService.self)!
    interactor.articlesCacheService = resolver.resolve(ArticlesCacheService.self)!

    viewController.output = presenter

    router.transitionHandler = viewController
  }
}

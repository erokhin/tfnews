//
//  ArticleDetailModuleInitializer.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class ArticleDetailModuleInitializer: NSObject {
  @IBOutlet private weak var viewController: ArticleDetailViewController!

  override func awakeFromNib() {
    let configurator = ArticleDetailModuleConfigurator()
    configurator.configure(viewController)
  }
}

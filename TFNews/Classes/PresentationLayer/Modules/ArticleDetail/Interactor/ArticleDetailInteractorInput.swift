//
//  ArticleDetailInteractorInput.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

protocol ArticleDetailInteractorInput: class {
  func configure(slug: String)
  func load()
  func retryLoading()
}

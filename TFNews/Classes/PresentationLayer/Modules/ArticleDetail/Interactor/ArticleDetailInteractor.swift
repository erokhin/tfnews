//
//  ArticleDetailInteractor.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class ArticleDetailInteractor {
  weak var output: ArticleDetailInteractorOutput?
  var articlesCacheService: ArticlesCacheService!
  var articlesService: ArticlesService!

  private var slug: String = ""
  private var request: Cancelable?
  private var cachedArticle: ArticleMO?
  private var contentNotLoaded = false

  private let htmlGenerator = ArticleHtmlGenerator()

  deinit {
    request?.cancel()
  }

  private func reloadArticle() {
    request = articlesService.loadArticle(slug: slug) { [weak self] result in
      switch result {
      case .success(let article):
        self?.articleLoaded(article)
      case .failure(let error):
        self?.articleLoadingFailed(error)
      }
    }
  }

  private func articleLoaded(_ article: Article) {
    output?.updateTitle(article.date)
    let html = htmlGenerator.generate(title: article.title, text: article.text)
    output?.updateState(.content(html))
    let incrementCounter = contentNotLoaded
    articlesCacheService.createOrUpdate(id: article.id, block: {
      $0.update(article, full: true)
      if incrementCounter {
        $0.counter += 1
      }
    }, completion: { [weak self] result in
      switch result {
      case .success:
        self?.contentNotLoaded = false
      case .failure(let error):
        print(error.localizedDescription)
      }
    })
  }

  private func articleLoadingFailed(_ error: Error) {
    if contentNotLoaded {
      output?.updateState(.retry)
    }
    print(error)
  }
}

// MARK: - ArticleDetailInteractorInput

extension ArticleDetailInteractor: ArticleDetailInteractorInput {
  func configure(slug: String) {
    self.slug = slug
  }

  func load() {
    cachedArticle = articlesCacheService.getArticle(slug: slug)
    output?.updateTitle(cachedArticle?.date)
    contentNotLoaded = !(cachedArticle?.text?.isEmpty == false)
    if contentNotLoaded {
      output?.updateState(.loading)
    } else {
      articlesCacheService.update(cachedArticle!, block: {
        $0.counter += 1
      }, completion: nil)
      let html = htmlGenerator.generate(title: cachedArticle?.title ?? "",
                                        text: cachedArticle?.text ?? "")
      output?.updateState(.content(html))
    }
    reloadArticle()
  }

  func retryLoading() {
    guard contentNotLoaded else { return }
    output?.updateState(.loading)
    reloadArticle()
  }
}

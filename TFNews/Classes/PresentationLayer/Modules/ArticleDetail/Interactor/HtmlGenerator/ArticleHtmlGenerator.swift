//
//  ArticleHtmlGenerator.swift
//  TFNews
//
//  Created by erokhin on 16/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class ArticleHtmlGenerator {

  private lazy var template: String = {
    guard let url = Bundle.main.url(forResource: "article", withExtension: "html") else {
      fatalError()
    }
    do {
      return try String(contentsOf: url)
    } catch {
      fatalError(error.localizedDescription)
    }
  }()

  func generate(title: String, text: String) -> String {
    return template.replacingOccurrences(of: "{title}", with: title)
      .replacingOccurrences(of: "{body}", with: text)
  }
}

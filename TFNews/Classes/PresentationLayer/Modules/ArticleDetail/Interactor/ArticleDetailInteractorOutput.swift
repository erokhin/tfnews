//
//  ArticleDetailInteractorOutput.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

protocol ArticleDetailInteractorOutput: class {
  func updateTitle(_ date: Date?)
  func updateState(_ state: ArticleDetailViewState)
}

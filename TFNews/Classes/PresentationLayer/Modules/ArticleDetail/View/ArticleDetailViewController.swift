//
//  ArticleDetailViewController.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import UIKit

final class ArticleDetailViewController: UIViewController {

  var output: ArticleDetailViewOutput!

  @IBOutlet private var activityIndicator: UIActivityIndicatorView!
  @IBOutlet private var webView: UIWebView!
  @IBOutlet private var retryView: UIView!

  override func viewDidLoad() {
    super.viewDidLoad()
    output.viewDidLoad()
  }

  @objc
  @IBAction private func didTapRetry() {
    output.didTapRetry()
  }
}

// MARK: - ArticleDetailViewInput

extension ArticleDetailViewController: ArticleDetailViewInput {
  func updateTitle(_ title: String) {
    navigationItem.title = title
  }

  func updateState(_ state: ArticleDetailViewState) {
    switch state {
    case .loading:
      activityIndicator.startAnimating()
      webView.isHidden = true
      retryView.isHidden = true
    case .content(let html):
      activityIndicator.stopAnimating()
      webView.isHidden = false
      webView.loadHTMLString(html, baseURL: nil)
      retryView.isHidden = true
    case .retry:
      activityIndicator.stopAnimating()
      webView.isHidden = true
      retryView.isHidden = false
    }
  }
}

// MARK: - UIWebViewDelegate

extension ArticleDetailViewController: UIWebViewDelegate {
  func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest,
               navigationType: UIWebViewNavigationType) -> Bool {
    switch navigationType {
    case .linkClicked:
      output.didTap(url: request.url!)
      return false
    case .other:
      return true
    default:
      return false
    }
  }
}

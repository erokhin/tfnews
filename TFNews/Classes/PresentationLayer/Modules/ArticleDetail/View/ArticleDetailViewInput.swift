//
//  ArticleDetailViewInput.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

enum ArticleDetailViewState {
  case loading
  case content(String)
  case retry
}

protocol ArticleDetailViewInput: class {
  func updateTitle(_ title: String)
  func updateState(_ state: ArticleDetailViewState)
}

//
//  ArticleDetailRouterInput.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

protocol ArticleDetailRouterInput: class {
  func open(url: URL)
}

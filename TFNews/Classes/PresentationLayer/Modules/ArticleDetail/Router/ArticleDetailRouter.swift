//
//  ArticleDetailRouter.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation
import SafariServices

final class ArticleDetailRouter {
  weak var transitionHandler: UIViewController?
}

// MARK: - ArticleDetailRouterInput

extension ArticleDetailRouter: ArticleDetailRouterInput {
  func open(url: URL) {
    let safariVC = SFSafariViewController(url: url)
    transitionHandler?.present(safariVC, animated: true)
  }
}

//
//  LaunchScreenPresenter.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class LaunchScreenPresenter {
  weak var view: LaunchScreenViewInput!
  var router: LaunchScreenRouterInput!
  var interactor: LaunchScreenInteractorInput!
}

// MARK: - LaunchScreenModuleInput

extension LaunchScreenPresenter: LaunchScreenModuleInput {

}

// MARK: - LaunchScreenViewOutput

extension LaunchScreenPresenter: LaunchScreenViewOutput {
  func viewDidLoad() {
    interactor.viewDidLoad()
  }
}

// MARK: - LaunchScreenInteractorOutput

extension LaunchScreenPresenter: LaunchScreenInteractorOutput {
  func ready() {
    router.openArticles()
  }
}

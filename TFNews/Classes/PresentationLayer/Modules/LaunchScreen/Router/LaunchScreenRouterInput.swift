//
//  LaunchScreenRouterInput.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

protocol LaunchScreenRouterInput: class {
  func openArticles()
}

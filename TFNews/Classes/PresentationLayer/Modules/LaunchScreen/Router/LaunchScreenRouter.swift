//
//  LaunchScreenRouter.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class LaunchScreenRouter {
  weak var transitionHandler: TransitionHandler!
}

// MARK: - LaunchScreenRouterInput

extension LaunchScreenRouter: LaunchScreenRouterInput {
  func openArticles() {
    transitionHandler.performSegue(identifier: "main", to: Any.self, completion: { _ in })
  }
}

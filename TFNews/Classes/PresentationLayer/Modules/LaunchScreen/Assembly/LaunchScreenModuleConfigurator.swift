//
//  LaunchScreenModuleConfigurator.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

struct LaunchScreenModuleConfigurator {
  func configure(_ viewController: LaunchScreenViewController) {
    let resolver = ApplicationAssembly.assembler.resolver
    let router = LaunchScreenRouter()
    let presenter = LaunchScreenPresenter()
    let interactor = LaunchScreenInteractor()

    presenter.router = router
    presenter.view = viewController
    presenter.interactor = interactor

    interactor.output = presenter
    interactor.coreDataStack = resolver.resolve(CoreDataStack.self)!

    viewController.output = presenter

    router.transitionHandler = viewController
  }
}

//
//  LaunchScreenModuleInitializer.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class LaunchScreenModuleInitializer: NSObject {
  @IBOutlet private weak var viewController: LaunchScreenViewController!

  override func awakeFromNib() {
    let configurator = LaunchScreenModuleConfigurator()
    configurator.configure(viewController)
  }
}

//
//  LaunchScreenInteractor.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class LaunchScreenInteractor {
  weak var output: LaunchScreenInteractorOutput!
  var coreDataStack: CoreDataStack!

  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  @objc
  private func coreDataStackReady() {
    NotificationCenter.default.removeObserver(self, name: .coreDataStackReady, object: nil)
    output.ready()
  }
}

// MARK: - LaunchScreenInteractorInput

extension LaunchScreenInteractor: LaunchScreenInteractorInput {
  func viewDidLoad() {
    if !coreDataStack.isLoaded {
      NotificationCenter.default.addObserver(self, selector: #selector(coreDataStackReady),
                                             name: .coreDataStackReady, object: nil)
    } else {
      output.ready()
    }
  }
}

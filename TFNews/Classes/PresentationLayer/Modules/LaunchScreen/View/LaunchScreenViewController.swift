//
//  LaunchScreenViewController.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import UIKit

final class LaunchScreenViewController: UIViewController {

  var output: LaunchScreenViewOutput!

  override func viewDidLoad() {
    super.viewDidLoad()
    output.viewDidLoad()
  }
}

// MARK: - LaunchScreenViewInput

extension LaunchScreenViewController: LaunchScreenViewInput {

}

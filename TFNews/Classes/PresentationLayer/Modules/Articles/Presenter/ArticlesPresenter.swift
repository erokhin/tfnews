//
//  ArticlesPresenter.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class ArticlesPresenter {
  weak var view: ArticlesViewInput!
  var router: ArticlesRouterInput!
  var interactor: ArticlesInteractorInput!
}

// MARK: - ArticlesModuleInput

extension ArticlesPresenter: ArticlesModuleInput {

}

// MARK: - ArticlesViewOutput

extension ArticlesPresenter: ArticlesViewOutput {
  func viewDidLoad() {
    interactor.viewDidLoad()
  }

  func didSelectArticle(at index: Int) {
    interactor.didSelectArticle(at: index)
  }

  func shouldBeginRefreshing() -> Bool {
    return interactor.beginRefreshing()
  }

  func didScrollToEnd() {
    interactor.loadMore()
  }

  func didTapRetry() {
    interactor.retryRefresh()
  }
}

// MARK: - ArticlesInteractorOutput

extension ArticlesPresenter: ArticlesInteractorOutput {

  func openArticle(slug: String) {
    router.openArticle(slug: slug)
  }

  func performBatchUpdate(_ batch: [CacheOperation]) {
    view.performBatchUpdate(batch)
  }

  func endRefreshing() {
    view.endRefreshing()
  }

  func updateState(_ state: ArticlesViewState) {
    view.updateState(state)
  }
}

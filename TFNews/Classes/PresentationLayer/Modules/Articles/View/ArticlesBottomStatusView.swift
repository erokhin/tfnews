//
//  ArticlesBottomStatusView.swift
//  TFNews
//
//  Created by erokhin on 16/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import UIKit

final class ArticlesBottomStatusView: UIView {
  @IBOutlet private var activityIndicator: UIActivityIndicatorView!
  @IBOutlet private var label: UILabel!

  var status: ContentLoadingStatus = .pending {
    didSet {
      guard status != oldValue else { return }
      switch status {
      case .error:
        label.text = NSLocalizedString("Ошибка загрузки :(", comment: "")
        activityIndicator.stopAnimating()
      case .finish:
        label.text = NSLocalizedString("Конец списка", comment: "")
        activityIndicator.stopAnimating()
      case .pending:
        label.text = ""
        activityIndicator.startAnimating()
      }
    }
  }

  override func awakeFromNib() {
    activityIndicator.startAnimating()
    label.text = ""
  }
}

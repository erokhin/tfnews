//
//  ArticlesViewOutput.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

protocol ArticlesDataSource: class {
  var numberOfArticles: Int { get }
  func article(at index: Int) -> ArticlePreview
}

protocol ArticlesViewOutput: class {
  func shouldBeginRefreshing() -> Bool
  func didScrollToEnd()
  func viewDidLoad()
  func didSelectArticle(at index: Int)
  func didTapRetry()
}

//
//  ArticlesViewInput.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

enum ArticlesViewState {
  /// Первичная загрузка
  case loading
  /// Ошибка загрузки, нужно дать пинка
  case retry
  /// Показываем какой-то контент
  case content(ContentLoadingStatus)
}

enum ContentLoadingStatus {
  /// Показываем индикатор загрузки в конце списка
  case pending
  // Список загружен полностью
  case finish
  // Ошибка загрузки
  case error
}

protocol ArticlesViewInput: class {
  func endRefreshing()
  func updateState(_ state: ArticlesViewState)
  func performBatchUpdate(_ batch: [CacheOperation])
}

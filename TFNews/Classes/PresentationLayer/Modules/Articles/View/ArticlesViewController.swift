//
//  ArticlesViewController.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import UIKit

final class ArticlesViewController: UIViewController {
  var output: ArticlesViewOutput!
  var dataSource: ArticlesDataSource!

  @IBOutlet private var tableView: UITableView!
  @IBOutlet private var activityIndicator: UIActivityIndicatorView!
  @IBOutlet private var retryView: UIView!
  @IBOutlet private var bottomStatusView: ArticlesBottomStatusView!

  private let refreshControl = UIRefreshControl()

  private var cellHeightCache: CellHeightCache!

  private let dateFormatter = DateFormatter()

  override func viewDidLoad() {
    super.viewDidLoad()
    configureTableView()
    output.viewDidLoad()
  }

  private func configureTableView() {
    dateFormatter.timeStyle = .none
    dateFormatter.dateStyle = .medium
    cellHeightCache = CellHeightCache(view: tableView)
    refreshControl.addTarget(self, action: #selector(beginRefreshing), for: .valueChanged)
  }

  @objc
  private func beginRefreshing() {
    if output.shouldBeginRefreshing() { return }
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
      self?.refreshControl.endRefreshing()
    }
  }

  @objc
  @IBAction private func didTapRetry() {
    output.didTapRetry()
  }
}

// MARK: - ArticlesViewInput

extension ArticlesViewController: ArticlesViewInput {
  func endRefreshing() {
    refreshControl.endRefreshing()
  }

  func performBatchUpdate(_ batch: [CacheOperation]) {
    UIView.performWithoutAnimation {
      tableView.beginUpdates()
      var tmp: [IndexPath] = [IndexPath(row: 0, section: 0)]
      batch.forEach {
        switch $0.type {
        case .insert:
          tmp[0].row = $0.newIndex
          cellHeightCache.insert(at: $0.newIndex)
          tableView.insertRows(at: tmp, with: .none)
        case .delete:
          tmp[0].row = $0.oldIndex
          cellHeightCache.delete(at: $0.oldIndex)
          tableView.deleteRows(at: tmp, with: .none)
        case .update:
          tmp[0].row = $0.oldIndex
          tableView.reloadRows(at: tmp, with: .none)
        case .move:
          cellHeightCache.move(at: $0.oldIndex, to: $0.newIndex)
          tmp[0].row = $0.oldIndex
          tableView.deleteRows(at: tmp, with: .none)
          tmp[0].row = $0.newIndex
          tableView.insertRows(at: tmp, with: .none)
        }
      }
      tableView.endUpdates()
    }
  }

  func updateState(_ state: ArticlesViewState) {
    switch state {
    case .content(let status):
      activityIndicator.stopAnimating()
      tableView.isHidden = false
      retryView.isHidden = true
      tableView.refreshControl = refreshControl
      bottomStatusView.status = status
    case .loading:
      activityIndicator.startAnimating()
      tableView.isHidden = true
      tableView.refreshControl = nil
      retryView.isHidden = true
    case .retry:
      activityIndicator.stopAnimating()
      tableView.isHidden = true
      tableView.refreshControl = nil
      retryView.isHidden = false
    }
  }
}

// MARK: - UITableViewDataSource

extension ArticlesViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataSource.numberOfArticles
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath) as? ArticleCell else {
      fatalError()
    }
    cell.dateFormatter = dateFormatter
    cell.configure(with: dataSource.article(at: indexPath.row))
    return cell
  }
}

// MARK: - UITableViewDelegate

extension ArticlesViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    output.didSelectArticle(at: indexPath.row)
  }

  func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    cellHeightCache.grow(to: indexPath.row)
    cellHeightCache[indexPath.row] = cell.frame.height
  }

  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return cellHeightCache[indexPath.row]
  }

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let maxOffset = scrollView.contentSize.height - scrollView.frame.height
    let delta = maxOffset - scrollView.contentOffset.y
    if delta < 200 {
      output.didScrollToEnd()
    }
  }
}

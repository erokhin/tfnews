//
//  ArticleCell.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import UIKit

final class ArticleCell: UITableViewCell {
  @IBOutlet private var titleLabel: UILabel!
  @IBOutlet private var dateLabel: UILabel!
  @IBOutlet private var counterLabel: UILabel!

  var dateFormatter: DateFormatter!

  func configure(with preview: ArticlePreview) {
    titleLabel.text = preview.title
    dateLabel.text = dateFormatter.string(from: preview.date)
    counterLabel.text = String(format: NSLocalizedString("Просмотров: %d", comment: ""), preview.counter)
  }
}

//
//  ArticlesRouter.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class ArticlesRouter {
  weak var transitionHandler: TransitionHandler!
}

// MARK: - ArticlesRouterInput

extension ArticlesRouter: ArticlesRouterInput {
  func openArticle(slug: String) {
    transitionHandler.performSegue(identifier: "detail", to: ArticleDetailModuleInput.self) { input in
      input.configure(slug: slug)
    }
  }
}

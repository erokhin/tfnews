//
//  ArticlesInteractorOutput.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

protocol ArticlesInteractorOutput: class {
  func openArticle(slug: String)
  func performBatchUpdate(_ batch: [CacheOperation])
  func endRefreshing()
  func updateState(_ state: ArticlesViewState)
}

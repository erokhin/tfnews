//
//  ArticlesInteractorInput.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

struct ArticlePreview {
  let title: String
  let date: Date
  let counter: Int
}

protocol ArticlesInteractorInput: class {
  func viewDidLoad()
  func beginRefreshing() -> Bool
  func didSelectArticle(at index: Int)
  func loadMore()
  func retryRefresh()
}

//
//  ArticlesCacheTrackerImpl.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import CoreData

final class ArticlesCacheTrackerImpl: NSObject, ArticlesCacheTracker {

  private let controller: NSFetchedResultsController<ArticleMO>

  var batchUpdateHandler: (([CacheOperation]) -> Void)?

  private var batch: [CacheOperation] = []

  init(context: NSManagedObjectContext) {
    let request: NSFetchRequest<ArticleMO> = ArticleMO.fetchRequest()
    request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
    request.predicate = NSPredicate(format: "isHidden = false")
    request.fetchBatchSize = 20
    controller = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context,
                                            sectionNameKeyPath: nil, cacheName: nil)
    super.init()
    controller.delegate = self
  }

  func reload() throws {
    try controller.performFetch()
  }

  var numberOfArticles: Int {
    return controller.sections?.first?.numberOfObjects ?? 0
  }

  func article(at index: Int) -> ArticleMO {
    return controller.object(at: IndexPath(row: index, section: 0))
  }
}

// MARK: - NSFetchedResultsControllerDelegate

extension ArticlesCacheTrackerImpl: NSFetchedResultsControllerDelegate {
  func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    batch = []
  }

  func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any,
                  at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
    batch.append(CacheOperation(type: type, oldIndex: indexPath?.row ?? -1, newIndex: newIndexPath?.row ?? -1))
  }

  func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    guard !batch.isEmpty else { return }
    batchUpdateHandler?(batch)
    batch = []
  }
}

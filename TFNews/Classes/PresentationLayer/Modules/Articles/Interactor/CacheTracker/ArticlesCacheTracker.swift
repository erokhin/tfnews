//
//  ArticlesCacheTracker.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation
import CoreData

typealias CacheOperationType = NSFetchedResultsChangeType

struct CacheOperation {
  let type: CacheOperationType
  let oldIndex: Int
  let newIndex: Int
}

protocol ArticlesCacheTracker: class {
  var batchUpdateHandler: (([CacheOperation]) -> Void)? { get set }
  var numberOfArticles: Int { get }
  func article(at index: Int) -> ArticleMO
  func reload() throws
}

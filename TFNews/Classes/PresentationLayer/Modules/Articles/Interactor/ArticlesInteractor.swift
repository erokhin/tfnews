//
//  ArticlesInteractor.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation
import CoreData

final class ArticlesInteractor: NSObject {
  weak var output: ArticlesInteractorOutput?
  var articlesService: ArticlesService!
  var cacheService: ArticlesCacheService!
  var cacheTracker: ArticlesCacheTracker!

  /// Идентификатор самой старой новости. Используется для определения пересечения страниц с новостями.
  private var lastArticleId: String = ""
  private let pageSize: Int = 20
  /// Ширина пересечения соседних страниц.
  private let overlapping: Int = 5
  private var actualOffset: Int = 0

  private var loadingRequest: Cancelable?
  private var refreshRequest: Cancelable?
  /// Выполняется подгрузка или обновление.
  private var isBusy = false
  /// Загрузили весь список.
  private var isFinish = false

  deinit {
    refreshRequest?.cancel()
    loadingRequest?.cancel()
  }

  private func completeRefresh(_ articles: [Article]) {
    cacheService.addPreviews(articles, reset: true) { [weak self] result in
      switch result {
      case .success:
        self?.lastArticleId = articles.last?.id ?? ""
        self?.actualOffset = articles.count
        self?.refreshCompleted()
      case .failure(let error):
        self?.refreshFailed(error)
      }
    }
  }

  private func refreshCompleted() {
    isBusy = false
    output?.endRefreshing()
    isFinish = false
    output?.updateState(.content(.pending))
  }

  private func refreshFailed(_ error: Error) {
    isBusy = false
    if cacheTracker.numberOfArticles == 0 {
      output?.updateState(.retry)
    }
    print(error)
    output?.endRefreshing()
  }
}

// MARK: - ArticlesInteractorInput

extension ArticlesInteractor: ArticlesInteractorInput {

  func viewDidLoad() {
    cacheTracker.batchUpdateHandler = { [weak self] batch in
      self?.output?.performBatchUpdate(batch)
    }
    do {
      try cacheTracker.reload()
      if cacheTracker.numberOfArticles == 0 {
        output?.updateState(.loading)
      } else {
        output?.updateState(.content(.pending))
      }
      beginRefreshing()
    } catch { // ???
      print(error.localizedDescription)
    }
  }

  @discardableResult func beginRefreshing() -> Bool {
    guard !isBusy else { return false }
    isBusy = true
    refreshRequest = articlesService.loadArticles(offset: 0, count: pageSize) { [weak self] result in
      self?.refreshRequest = nil
      switch result {
      case .success(let page):
        self?.completeRefresh(page.news)
      case .failure(let error):
        self?.refreshFailed(error)
      }
    }
    return true
  }

  func loadMore() {
    guard !isBusy && !isFinish && !lastArticleId.isEmpty else { return }
    isBusy = true
    let offset = max(0, actualOffset - overlapping)
    loadingRequest = articlesService.loadArticles(offset: offset, count: pageSize) { [weak self] result in
      self?.loadingRequest = nil
      switch result {
      case .success(let page):
        self?.completeLoading(page.news)
      case .failure(let error):
        self?.loadingFailed(error)
      }
    }
  }

  func retryRefresh() {
    if cacheTracker.numberOfArticles == 0 {
      if beginRefreshing() {
        output?.updateState(.loading)
      }
    } else {
      output?.updateState(.content(.pending))
    }
  }

  private func completeLoading(_ articles: [Article]) {
    var articles = articles
    if let index = articles.index(where: { $0.id == lastArticleId }) {
      articles.removeSubrange(0 ... index)
    } else if articles.isEmpty { // Конец ленты.
      return loadingCompleted(end: true)
    } else { // Серьезные изменения в ленте?
      loadingCompleted(end: false)
      beginRefreshing()
      return
    }
    cacheService.addPreviews(articles, reset: false) { [weak self] result in
      switch result {
      case .success:
        self?.lastArticleId = articles.last?.id ?? ""
        self?.actualOffset += articles.count
        self?.loadingCompleted(end: articles.isEmpty)
      case .failure(let error):
        self?.loadingFailed(error)
      }
    }
  }

  private func loadingCompleted(end: Bool) {
    isBusy = false
    isFinish = end
    output?.updateState(.content(end ? .finish : .pending))
  }

  private func loadingFailed(_ error: Error) {
    if case URLError.notConnectedToInternet = error {
      // Защита от шквала запросов в случае отсутствия сети
      DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
        self?.isBusy = false
      }
    } else {
      isBusy = false
    }
    output?.updateState(.content(.error))
  }

  func didSelectArticle(at index: Int) {
    let article = cacheTracker.article(at: index)
    guard let slug = article.slug else { return }
    output?.openArticle(slug: slug)
  }
}

// MARK: - ArticlesDataSource

extension ArticlesInteractor: ArticlesDataSource {

  var numberOfArticles: Int {
    return cacheTracker.numberOfArticles
  }

  func article(at index: Int) -> ArticlePreview {
    let article = cacheTracker.article(at: index)
    return ArticlePreview(title: article.title ?? "", date: article.date ?? Date(),
                          counter: Int(article.counter))
  }
}

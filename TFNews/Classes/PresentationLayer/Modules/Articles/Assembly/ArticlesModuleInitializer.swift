//
//  ArticlesModuleInitializer.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

final class ArticlesModuleInitializer: NSObject {
  @IBOutlet private weak var viewController: ArticlesViewController!

  override func awakeFromNib() {
    let configurator = ArticlesModuleConfigurator()
    configurator.configure(viewController)
  }
}

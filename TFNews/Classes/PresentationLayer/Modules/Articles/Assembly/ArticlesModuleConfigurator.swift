//
//  ArticlesModuleConfigurator.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

struct ArticlesModuleConfigurator {
  func configure(_ viewController: ArticlesViewController) {
    let resolver = ApplicationAssembly.assembler.resolver
    let router = ArticlesRouter()
    let presenter = ArticlesPresenter()
    let interactor = ArticlesInteractor()

    presenter.router = router
    presenter.view = viewController
    presenter.interactor = interactor

    interactor.output = presenter
    interactor.articlesService = resolver.resolve(ArticlesService.self)!
    interactor.cacheService = resolver.resolve(ArticlesCacheService.self)!
    interactor.cacheTracker = ArticlesCacheTrackerImpl(context: resolver.resolve(CoreDataStack.self)!.mainContext)

    viewController.output = presenter
    viewController.dataSource = interactor
    router.transitionHandler = viewController
  }
}

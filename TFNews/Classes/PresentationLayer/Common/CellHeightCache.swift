//
//  CellHeightCache.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import UIKit

struct CellHeight {
  var portrait: CGFloat
  var landscape: CGFloat
}

extension CellHeight {
  static let auto = CellHeight(portrait: UITableViewAutomaticDimension, landscape: UITableViewAutomaticDimension)
}

struct CellHeightCache {

  private var items: [CellHeight] = []
  private let view: UIView

  private var isLandscape: Bool {
    return view.frame.width > view.frame.height
  }

  subscript(index: Int) -> CGFloat {
    get {
      guard index < items.count else { return UITableViewAutomaticDimension }
      return isLandscape ? items[index].landscape : items[index].portrait
    }
    set {
      if isLandscape {
        items[index].landscape = newValue
      } else {
        items[index].portrait = newValue
      }
    }
  }

  init(view: UIView) {
    self.view = view
  }

  mutating func grow(to size: Int) {
    while items.count <= size {
      items.append(.auto)
    }
  }

  mutating func move(at oldIndex: Int, to newIndex: Int) {
    grow(to: max(oldIndex, newIndex))
    items.insert(items.remove(at: oldIndex), at: newIndex)
  }

  mutating func insert(_ cellHeight: CellHeight = .auto, at index: Int) {
    guard items.count >= index else { return }
    items.insert(cellHeight, at: index)
  }

  mutating func delete(at index: Int) {
    guard items.count > index else { return }
    items.remove(at: index)
  }
}

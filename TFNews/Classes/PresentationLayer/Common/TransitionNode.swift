//
//  TransitionNode.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import UIKit

protocol TransitionNode: class {
  func configure(_ segue: UIStoryboardSegue)
}

final class GenericTransitionNode<T>: TransitionNode {

  typealias CompletionHandler = (T) -> Void

  let completion: CompletionHandler

  init(completion: @escaping CompletionHandler) {
    self.completion = completion
  }

  func configure(_ segue: UIStoryboardSegue) {
    let viewController = segue.destination
    if let input: T = value(for: "output", in: Mirror(reflecting: viewController)) {
      completion(input)
    } else if let input = viewController as? T {
      completion(input)
    } else {
      fatalError()
    }
  }
}

private func value<T>(for propertyName: String, in mirror: Mirror) -> T? {
  for property in mirror.children where property.label == propertyName {
    return property.value as? T
  }
  if let superclassMirror = mirror.superclassMirror {
    return value(for: propertyName, in: superclassMirror)
  }
  return nil
}

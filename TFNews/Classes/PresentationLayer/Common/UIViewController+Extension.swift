//
//  UIViewController+Extension.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import UIKit

extension UIViewController: TransitionHandler {

  private static let swizzlePrepareForSegue: Void = {
    let originalSel = #selector(UIViewController.prepare(for:sender:))
    let swizzledSel = #selector(UIViewController.swizzledPrepare(for:sender:))

    let instanceClass = UIViewController.self
    let originalMethod = class_getInstanceMethod(instanceClass, originalSel)!
    let swizzledMethod = class_getInstanceMethod(instanceClass, swizzledSel)!

    let added = class_addMethod(instanceClass, originalSel, method_getImplementation(swizzledMethod),
                                method_getTypeEncoding(swizzledMethod))
    if added {
      class_replaceMethod(instanceClass, swizzledSel, method_getImplementation(originalMethod),
                          method_getTypeEncoding(originalMethod))
    } else {
      method_exchangeImplementations(originalMethod, swizzledMethod)
    }
  }()

  @objc
  private func swizzledPrepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let node = sender as? TransitionNode {
      node.configure(segue)
    } else {
      swizzledPrepare(for: segue, sender: sender)
    }
  }

  final func performSegue<T>(identifier: String, to type: T.Type, completion: @escaping (T) -> Void) {
    UIViewController.swizzlePrepareForSegue
    let node = GenericTransitionNode(completion: completion)
    performSegue(withIdentifier: identifier, sender: node)
  }
}

//
//  TransitionHandler.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

protocol TransitionHandler: class {
  func performSegue<T>(identifier: String, to type: T.Type, completion: @escaping (T) -> Void)
}

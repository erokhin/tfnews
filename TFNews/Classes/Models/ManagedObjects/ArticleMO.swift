//
//  ArticleMO.swift
//  TFNews
//
//  Created by erokhin on 15/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import CoreData

@objc(ArticleMO)
final class ArticleMO: NSManagedObject {

  func update(_ article: Article, full: Bool = false) {
    id = article.id
    title = HTMLMarkupFilter().filter(article.title)
    slug = article.slug
    updatedTime = article.updatedTime
    date = article.date
    isHidden = article.hidden || article.deleted
    if full {
      text = article.text
    }
  }
}

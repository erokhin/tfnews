//
//  ArticlesPage.swift
//  TFNews
//
//  Created by erokhin on 14/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

struct ArticlesPage: Codable {
  var news: [Article]
  var total: Int
}

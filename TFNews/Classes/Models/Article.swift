//
//  Article.swift
//  TFNews
//
//  Created by erokhin on 11/07/2018.
//  Copyright © 2018 erokhin. All rights reserved.
//

import Foundation

struct Article: Codable {
  var id: String = ""
  var title: String = ""
  var text: String = ""
  var slug: String = ""
  var updatedTime: Date
  var date: Date
  var hidden: Bool = false
  var deleted: Bool = false
}
